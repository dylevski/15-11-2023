﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    cout << "Enter n." << endl;
    cin >> n;
    int k = 1;
    double ans = sqrt(2);
    while (k < n)
    {
        ans = sqrt(2 + ans);
        k = k + 1;
    }
    cout << ans;
}