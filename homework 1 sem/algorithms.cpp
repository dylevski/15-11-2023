#include <iostream>
#include "algorithms.h"

void swap(int& a, int& b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

void swap(char& a, char& b)
{
    char tmp = a;
    a = b;
    b = tmp;
}

void bubbleSort(int numbers[], int length)
{
    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < length-i-1; j++)
        {
            if (numbers[j]>numbers[j+1])
            {
                swap(numbers[j], numbers[j+1]);
            }
        }
    }
}

void selectionSort(int numbers[], int length)
{
    int currentMinIndex;
    for (int i = 0; i < length; i++)
    {
        currentMinIndex = i;
        for (int j = i+1; j < length; j++)
        {
            if (numbers[currentMinIndex]>numbers[j])
            {
                currentMinIndex = j;
            }
        }
        swap(numbers[currentMinIndex], numbers[i]);
    }
}

void displayArray(int arr[], int length)
{
    for (int i = 0; i < length; i++)
    {
        std::cout << arr[i] << " ";
    }
}

void displayArray(char arr[])
{
    int length = lengthArray(arr);
    for (int i = 0; i < length; i++)
    {
        std::cout << arr[i] << " ";
    }
}

int binarySearch(int arr[], int searchedElement, int length)
{
    int firstIndex = 0;
    int lastIndex = length - 1;

    while (firstIndex < lastIndex)
    {
        int middleIndex = (lastIndex + firstIndex) / 2;

        if (searchedElement < arr[middleIndex])
        {
            lastIndex = middleIndex - 1;
        }
        else if (searchedElement > arr[middleIndex])
        {
            firstIndex = middleIndex + 1;
        }
        else
        {
            return middleIndex;
        }
    }

    if (firstIndex == lastIndex)
    {
        if (arr[lastIndex] == searchedElement)
        {
            return lastIndex;
        }
        else
        {
            return -1;
        }
    }
}

int lengthArray(char arr[])
{
    int i = 0;
    for (; arr[i]; i++)
    {

    }
    return i;
}

int firstEntrance(char string[], char substring[])
{
    int entranceIndex = 0;
    bool isEnter = false;
    for (int i = 0; string[i]; i++)
    {
        for (int j = 0; substring[j]; j++)
        {
            if (substring[j] == string[i + j])
            {
                isEnter = true;
                entranceIndex = i;
                if (!substring[j + 1])
                {
                    return entranceIndex;
                }
            }
            else
            {
                isEnter = false;
                entranceIndex = 0;
                break;
            }
        }
    }

    return -1;
}