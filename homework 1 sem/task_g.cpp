﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    cout << "Enter n." << endl;
    cin >> n;
    double ans = 0, den = 0, fac = 1;
    int k = 1;
    while (k<=n)
    {
        fac *= k;
        den += 1./k;
        ans += fac / den;
        k++;
    }
    cout << ans;
}