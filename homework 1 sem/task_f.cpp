﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    cout << "Enter n." << endl;
    cin >> n;
    int k = 1;
    double ans = 0, a = 1;
    while (k <= n)
    {
        a = -a;
        ans = ans + (a / (2*k + 1));
        k = k + 1;
    }
    cout << ans;
}