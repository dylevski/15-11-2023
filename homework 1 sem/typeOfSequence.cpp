﻿#include <iostream> 
using namespace std;
void typeOfSequence(int number);

int main()
{
    int number;
    cin >> number;
    typeOfSequence(number);
}
void typeOfSequence(int number)
{
    number = number < 0 ? abs(number) : number;
    bool lessThan = false;
    bool moreThan = false;
    bool equals = false;
    int temp = number;
    int next = number % 10;
    number /= 10;
    while (number)
    {
        int previous = number % 10;
        if (next < previous)
        {
            moreThan = true;
        }
        else if (previous == next)
        {
            equals = true;
        }
        else
        {
            lessThan = true;
        }
        next = previous;
        number /= 10;
    }

    if (equals && !moreThan && !lessThan)
    {
        cout << temp << " Monotonic" << endl;
    }
    else if (equals && moreThan && !lessThan)
    {
        cout << temp << " Decreasing" << endl;
    }
    else if (!equals && moreThan && !lessThan)
    {
        cout << temp << " Strictly Decreasing" << endl;
    }
    else if (equals && !moreThan && lessThan)
    {
        cout << temp << " Increasing" << endl;
    }
    else if (!equals && !moreThan && lessThan)
    {
        cout << temp << " Strictly Increasing" << endl;
    }
    else if (!equals && !moreThan && !lessThan)
    {
        cout << temp << " one digit " << endl;
    }
    else
    {
        cout << temp << " Unsorted " << endl;
    }
}