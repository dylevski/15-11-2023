﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    cout << "Enter n." << endl;
    cin >> n;
    int k = 1;
    double ans = 1;
    while (k <= n)
    {
        ans = ans*( 1 + (1. / (k*k)) );
        k = k + 1;
    }
    cout << ans;
}