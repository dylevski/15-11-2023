﻿#include <iostream>
using namespace std;

int main()
{
    double x;
    cout << "Enter number: ";
    cin >> x;
    if (x <= 0)
        x = -x;
    else if (0 < x < 2)
        x = x * x;
    else
        x = 4;
    cout << "Your number is " << x;
}