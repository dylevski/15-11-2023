﻿#include <iostream>
using namespace std;
enum Allergens : unsigned char
{
    eggs = 1,
    peanuts = 2,
    shellfish = 4,
    strawberries = 8,
    tomatoes = 16,
    chocolate = 32,
    pollen = 64,
    cats = 128
};
void allergens(int score);
bool isAllergicTo(int score, Allergens allergen);

int main()
{
    cout << "Enter the score less than 256.\n";
    int score;
    cin >> score;
    cout << "Result for score " << score << ":";
    allergens(score);
}

void allergens(int score)
{
    if (isAllergicTo(score, eggs)) cout << "\neggs";
    if (isAllergicTo(score, peanuts)) cout << "\npeanuts";
    if (isAllergicTo(score, shellfish)) cout << "\nshellfish";
    if (isAllergicTo(score, strawberries)) cout << "\nstrawberries";
    if (isAllergicTo(score, tomatoes)) cout << "\ntomatoes";
    if (isAllergicTo(score, chocolate)) cout << "\nchocolate";
    if (isAllergicTo(score, pollen)) cout << "\npollen";
    if (isAllergicTo(score, cats)) cout << "\ncats";
}

bool isAllergicTo(int score, Allergens allergen)
{
    return score & allergen; 
}