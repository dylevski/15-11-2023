﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    cout << "Enter n." << endl;
    cin >> n;
    int k = 1;
    double ans = 0, den = 0;
    while (k <= n)
    {
        den += sin(k*3.14159/180);
        ans += 1./den;
        k++;
    }
    cout << ans;
}