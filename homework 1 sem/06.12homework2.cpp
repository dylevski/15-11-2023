﻿#include <iostream>
using namespace std;
void random(int [], int);
int largerThanN(int [], int n, int k);

int main()
{
    const int k = 50;
    int arr[k]{}, n;
    random(arr, k);
    cout << "\nEnter number:\n";
    cin >> n;
    cout << largerThanN(arr, n, k);
}

void random(int arr[], int k)
{
    srand(time(NULL));
    for (int i = 0; i <= k; i++)
    {
        arr[i] = rand() % 100;
        cout << arr[i] << ", ";
    }
}

int largerThanN(int arr[], int n, int k)
{
    int count=0;
    for (int i = 0; i < k; i++)
    {
        if (arr[i]>n)
        {
            count++;
        }
    }
    return count;
}
