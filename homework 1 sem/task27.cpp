﻿#include <iostream>
using namespace std;

int main()
{
    double x;
    int y;
    cout << "Enter number: ";
    cin >> x;
    if (x < 0)
        y = 0;
    else
    {
        y = floor (x);
        y = y % 2;
        if (y == 0)
            y = 1;
        else
            y = -1;
    }
    cout << "Your number is " << y;
}