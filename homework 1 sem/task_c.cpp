﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    cout << "Enter n." << endl;
    cin >> n;
    int k = 1;
    double ans = 0;
    while (k <= n)
    {
        ans = ans + (1. / pow(k,5));
        k = k + 1;
    }
    cout << ans;
}