﻿#include <iostream> 

using namespace std;

char ToUpper(char);
bool IsAlfa(char);
bool IsUppperAlfa(char);
bool Equals(char lhs[], char rhs[], bool caseSensetive);

int main()
{
    char str1[] = "HELLO WORLD", str2[] = "HELLO WOrlD", str3[] = "HELLO WOrlD";
    cout << Equals(str1, str2, 1) << endl;
    cout << Equals(str1, str2, 0) << endl;
    cout << Equals(str2, str3, 1) << endl;
    cout << Equals(str2, str3, 0) << endl;

}

char ToUpper(char symbol)
{
    //if (symbol >= 'a' && symbol <= 'z')
    //{
    //    return symbol - 32;
    //}
    //return symbol;
    return symbol >= 'a' && symbol <= 'z' ? symbol - 32 : symbol;
}

bool IsAlfa(char symbol)
{
    return symbol >= 'A' && symbol <= 'Z' || symbol >= 'a' && symbol <= 'z';
}

bool IsUppperAlfa(char symbol)
{
    return symbol >= 'A' && symbol <= 'Z';

}

bool Equals(char lhs[], char rhs[], bool caseSensetive)
{
    if (strlen(lhs) != strlen(rhs))
    {
        return false;
    }
    for (int i = 0; lhs[i]; i++)
    {
        char tmpSymbolFirst = caseSensetive ? lhs[i] : ToUpper(lhs[i]);
        char tmpSymbolSecond = caseSensetive ? rhs[i] : ToUpper(rhs[i]);
        //if (!caseSensetive)
        //{
        //    tmpSymbolFirst = ToUpper(lhs[i]);
        //    tmpSymbolSecond = ToUpper(rhs[i]);
        //}
        if (tmpSymbolFirst != tmpSymbolSecond)
        {
            return false;
        }
    }
    return true;
}