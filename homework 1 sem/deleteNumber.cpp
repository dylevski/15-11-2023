﻿#include <iostream>
using namespace std;
int deleteNumbers(int number, int numeral);

int main()
{
    int number;unsigned int numeral;
    cout << "Enter number" << endl;
    cin >> number;
    cout << "Enter numeral" << endl;
    cin >> numeral;
    if (numeral>9)
    {
        cout << "Error.";
    }
    else
    {
        cout << deleteNumbers(number, numeral);
    }
}

int deleteNumbers(int number, int numeral)
{
    int k, newNumber = 0;
    for (int i = 0; number >= 1; i++)
    {
        k = number % 10;
        number /= 10;
        if (k != numeral)
        {
            newNumber += k * pow(10, i);
        }
        else
        {
            i--;
        }
    }
    return newNumber;
}
