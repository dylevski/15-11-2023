﻿#include <iostream>
using namespace std;
void random(int (& arr)[20], int);
int sumA(int(&arr)[20], int n);
int sumB(int(&arr)[20], int n);
int sumC(int(&arr)[20], int n);
bool IsPrime(int i);

int main()
{
    const int n=20;
    int numbers[n]{ 0 };
	random(numbers, n);
	//srand(time(NULL));
	//for (int i = 0; i <= n; i++)
	//{
	//	numbers[i] = rand() % 100;
	//	cout << numbers[i] << ", ";
	//}

	cout << "\n1. " << sumA(numbers, n);
	cout << "\n2. " << sumB(numbers, n);
	cout << "\n3. " << sumC(numbers, n);
}

int sumA(int(& numbers)[20], int n)
{
	int summ = 0;
	for (int i = 0; i <= n; i = i + 2)
	{
		if (numbers[i] < 0)
		{
			summ += numbers[i];
		}
	}
	return summ;
}

int sumB(int(&numbers)[20], int n)
{
	int summ = 0;
	for (int i = 0; i <= n; i++)
	{
		if (numbers[i] > 0 && IsPrime(i))
		{
			summ += numbers[i];
		}
	}
	return summ;
}

int sumC(int(&numbers)[20], int n) {
	int summ = 0;
	for (int i = 0; i <= n; i++)
	{
		if (IsPrime(numbers[i]))
		{
			summ += numbers[i];
		}
	}
	return summ;
}

void random(int (& numbers)[20], int n)
{
	srand(time(NULL));
	for (int i = 0; i <= n; i++)
	{
		numbers[i] = rand() % 100 - 50;
		cout << numbers[i] << ", ";
	}
}

bool IsPrime(int i)
{
	if (i <= 1)
	{
		return false;
	}
	for (int k = 2; k <= sqrt(i); k++) {
		if (i % k == 0) 
		{
			return false;
		}
	}
	return true;
}