﻿#include <iostream>
using namespace std;
void FibonacciSequence(int count);

int main()
{
    cout << "Enter count of numbers of Fibonacci sequence.\n";
    int count;
    cin >> count;
    FibonacciSequence(count);
}

void FibonacciSequence(int count)
{
    unsigned int first = 0, second = 1, i = 2;
    if (count < 2)
    {
        cout << "0";
    } 
    else {
        cout << "0, 1";
        while (i < count)
        {
            unsigned int newNumber = first + second;
            cout << ", " << newNumber;
            first = second;
            second = newNumber;
            i++;
        }
    }
}